from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Student

def index(request):
    return HttpResponse("Student Service Index")



def addStudent(data):
	print("#View Student -> addStudent")
	try:
		if data.get('age', None):
			age = int(data.get('age', None))
		else:
			age = 18
		student = Student.objects.create(first_name=data.get('first_name', None), last_name=data.get('last_name', None),
									   email=data.get('email', None), age=age, grade=data.get('grade', None))
		response = {"state": True, 'data': student.to_json()}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def getAllStudents():
	print("#View Student -> GetAllStudents")
	try:
		response = list()
		students = Student.objects.all()
		for student in students:
			response.append(student.to_json())
		response = {"state": True, "data": response}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def updateStudent(id, data):
	print("#View Student -> updateStudent")
	try:
		student = Student.objects.filter(id=id).update(first_name=data.get('first_name', None),
														   last_name=data.get('last_name', None),
									   					   email=data.get('email', None), age=data.get('age', None),
									   					   grade=data.get('grade', None))
		response = {"state": True, 'idUpdate': id}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def deleteStudent(id):
	print("#View Student -> deleteStudent")
	try:
		student = Student.objects.get(id=id).delete()
		response = {"state": True, 'idDelete':id}
	except Exception as e:
		response = {"state": False, 'error': str(e)}
	return response


@csrf_exempt
def student(request):
	if request.method == 'POST':
		response = addStudent(request.POST)
	elif request.method == 'GET':
		response = getAllStudents()
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)


@csrf_exempt
def student_actions(request, studentId):
	if request.method == 'DELETE':
		response = deleteStudent(studentId)
	elif request.method == 'POST':
		response = updateStudent(studentId, request.POST)
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)
