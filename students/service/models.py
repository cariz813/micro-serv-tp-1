from django.db import models

# Create your models here.


class Student(models.Model):
    first_name = models.CharField(max_length=30, blank=False, null=False)
    last_name = models.CharField(max_length=30, blank=False, null=False)
    email = models.CharField(max_length=30, blank=False, null=False)
    age = models.IntegerField(default=18)
    grade = models.CharField(max_length=30, default="Bachelor 1")

    class Meta:
    	unique_together = [['first_name', 'last_name']]

    def to_json(self):
    	return {
    		"id": self.id,
    		"first_name": self.first_name,
    		"last_name": self.last_name,
    		"email": self.email,
    		"age": self.age,
    		"grade": self.grade,
    	}
