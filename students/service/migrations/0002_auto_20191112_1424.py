# Generated by Django 2.2.7 on 2019-11-12 14:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='student',
            unique_together={('first_name', 'last_name')},
        ),
    ]
