from django.db import models

# Create your models here.


class Lesson(models.Model):
    name = models.CharField(max_length=30, blank=False, null=False, unique=True)
    desc = models.CharField(max_length=30, blank=False, null=False)
    moduls_nb = models.IntegerField(blank=False, null=False)

    # class Meta:
    	# unique_together = [['first_name', 'last_name']]

    def to_json(self):
    	return {
    		"id": self.id,
    		"name": self.name,
    		"desc": self.desc,
    		"moduls_nb": self.moduls_nb,
    	}
