from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Lesson

def index(request):
    return HttpResponse("Lesson Service Index")



def addLesson(data):
	print("#View Lesson -> addLesson")
	try:
		if data.get('moduls_nb', None):
			moduls = int(data.get('moduls_nb', None))
		else:
			moduls = 1
		lesson = Lesson.objects.create(name=data.get('name', None), desc=data.get('desc', None),
									   moduls_nb=moduls)
		response = {"state": True, 'data': lesson.to_json()}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def getAllLessons():
	print("#View Lesson -> GetAllLessons")
	try:
		response = list()
		lessons = Lesson.objects.all()
		for lesson in lessons:
			response.append(lesson.to_json())
		response = {"state": True, "data": response}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def updateLesson(id, data):
	print("#View Lesson -> updateLesson")
	try:
		if data.get('moduls_nb', None):
			moduls = int(data.get('moduls_nb', None))
			Lesson.objects.filter(id=id).update(name=data.get('name', None), desc=data.get('desc', None),
									   				 	 moduls_nb=moduls)
		else:
			Lesson.objects.filter(id=id).update(name=data.get('name', None), desc=data.get('desc', None))
		response = {"state": True, 'idUpdate': id}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def deleteLesson(id):
	print("#View Lesson -> deleteLesson")
	try:
		lesson = Lesson.objects.get(id=id).delete()
		response = {"state": True, 'idDelete':id}
	except Exception as e:
		response = {"state": False, 'error': str(e)}
	return response


@csrf_exempt
def lesson(request):
	if request.method == 'POST':
		response = addLesson(request.POST)
	elif request.method == 'GET':
		response = getAllLessons()
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)


@csrf_exempt
def lesson_actions(request, lessonId):
	if request.method == 'DELETE':
		response = deleteLesson(lessonId)
	elif request.method == 'POST':
		response = updateLesson(lessonId, request.POST)
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)
