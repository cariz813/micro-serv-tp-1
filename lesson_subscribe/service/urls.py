from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('subscribe', views.subscribe, name='subscribe'),
    path('subscribe/<int:subscribeId>', views.subscribe_actions, name='subscribe_actions'),
    path('add_notation', views.add_notation, name='add_notation'),
    path('coverage/<int:lessonId>', views.get_coverage, name='get_coverage'),
]