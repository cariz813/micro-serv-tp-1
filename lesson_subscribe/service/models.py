from django.db import models

# Create your models here.


class Subscribe(models.Model):
    date = models.DateTimeField(auto_now=True)
    student = models.IntegerField(blank=False, null=False)
    lesson = models.IntegerField(blank=False, null=False)

    class Meta:
    	unique_together = [['student', 'lesson']]

    def to_json(self):
    	return {
    		"id": self.id,
    		"date": str(self.date),
    		"student": self.student,
    		"lesson": self.lesson
    	}


class Notation(models.Model):
    date = models.DateTimeField(auto_now=True)
    subscribe = models.ForeignKey('Subscribe', on_delete=models.CASCADE)
    note = models.IntegerField(blank=False, null=False)

    def to_json(self):
        return {
            "id": self.id,
            "date": str(self.date),
            "subscribe": self.subscribe,
            # "lesson": self.lesson,
            "note": self.note
        }


